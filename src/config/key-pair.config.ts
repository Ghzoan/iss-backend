export default {
  algorithm: 'rsa',
  options: {
    modulusLength: 530,
    publicExponent: 0x10101,
    publicKeyEncoding: {
      type: 'pkcs1',
      format: 'der'
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'der',
      cipher: 'aes-192-cbc',
      passphrase: 'ISS PROJECT'
    }
  }
}
