import { HttpModule, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./Entities/user-entity";
import { UserService } from "./user/user.service";
import { UserGateway } from "./user/user.gateway";
import { UserPasswordEntity } from "./Entities/user-password-entity";
import { GatewayPayloadTransformPipe } from "./user/pipes/gateway-payload-transform.pipe";
import { CryptHandlerService } from "./user/crypt-handler.service";

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      database: 'iss',
      entities: ['dist/**/*-entity.js'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      User,
      UserPasswordEntity,
    ]),
    HttpModule
  ],
  controllers: [],
  providers: [UserService, UserGateway, GatewayPayloadTransformPipe, CryptHandlerService],
})
export class AppModule {}
