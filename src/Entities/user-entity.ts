import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { UserPasswordEntity } from "./user-password-entity";
@Entity()
export class User{

  @PrimaryGeneratedColumn()
  id: number

  @Column({unique: true})
  name: string

  @Column()
  password: string

  @Column()
  created_at: Date

  @Column({type: "longtext", nullable: true})
  public_key: string

  @OneToMany(() => UserPasswordEntity, password => password.user)
  passwords: Promise<UserPasswordEntity[]>

}
