import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user-entity";

@Entity('passwords')
export class UserPasswordEntity{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  user_id: number;

  @Column()
  title: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column('longtext')
  description: string;

  @ManyToOne(() => User, user => user.passwords)
  user: Promise<User>;
}
