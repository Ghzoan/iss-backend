import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';
import { CryptHandlerService } from "../crypt-handler.service";

@Injectable()
export class GatewayPayloadTransformPipe implements PipeTransform {
  constructor(private readonly _cryptHandlerService: CryptHandlerService) {
  }
  transform(value: any, metadata: ArgumentMetadata) {
    if( !value.nsp ){
    }
    return value;
  }
}
