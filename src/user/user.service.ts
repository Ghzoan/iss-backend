import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "../Entities/user-entity";
import { Repository } from "typeorm";
import { CreateUserDto } from "./dto/create-user.dto";
import { LoginUserDto } from "./dto/login-user.dto";
import { UserPasswordEntity } from "../Entities/user-password-entity";
import { CreatePasswordDto } from "./dto/create-password.dto";
import { EditPasswordDto } from "./dto/edit-password.dto";
import { ViewDeletePasswordDto } from "./dto/view-delete-password.dto";

@Injectable()
export class UserService{

  /**
   * Create a new instance form UserService
   *
   * @constructor
   *
   * @param _userRepository {Repository<User>}
   * @param _passwordRepository {Repository<UserPasswordEntity>}
   */
  constructor(
    @InjectRepository(User) private _userRepository: Repository<User>,
    @InjectRepository(UserPasswordEntity) private _passwordRepository: Repository<UserPasswordEntity>
  ) {
  }

  /**
   * Create a new user
   *
   * @param userPayload {CreateUserDto}
   *
   * @return {Promise<User>}
   *
   */
  public createUser(userPayload: CreateUserDto): Promise<User>{
    // Get a new instance form entity.
    const userInst = this._userRepository.create({...userPayload, created_at: new Date()});
    // Save instance to database
    return this._userRepository.save(userInst);
  }

  /**
   * Login old user
   *
   * @param userPayload {LoginUserDto}
   *
   * @return {Promise<User|boolean>}
   *
   */
  public loginOldUser(userPayload: any): Promise<User|boolean>{
    return new Promise<User | boolean>((resolve, reject) => {
      this._userRepository.find({
        where: [userPayload]
      }).then(users => {
        if (!users.length){
          resolve(false)
        }
        resolve(users[0])
      }).catch(e => reject(e))
    })
  }

  /**
   * Create a new password for user
   *
   * @param passwordPayload {CreatePasswordDto}
   *
   * @return {Promise<UserPasswordEntity>}
   *
   */
  public createNewPassword(passwordPayload: CreatePasswordDto): Promise<UserPasswordEntity>{
    // Get password instance.
    const passwordInst = this._passwordRepository.create(passwordPayload);
    // Save instance to database.
    return this._passwordRepository.save(passwordInst);
  }

  /**
   * Edit password details
   *
   * @param passwordPayload {EditPasswordDto}
   *
   * @return {Promise<any>}
   */
  public editPassword(passwordPayload: EditPasswordDto): Promise<any>{
    return new Promise<any>(async (resolve, reject) => {
      const {id} = passwordPayload;
      delete passwordPayload['id'];
      this._passwordRepository.update(id, passwordPayload)
        .then(res => resolve(res))
        .catch(e => reject(e))
    })
  }

  /**
   * Delete password
   *
   * @param passwordPayload {ViewDeletePasswordDto}
   *
   * @return {Promise<any>}
   *
   */
  public deletePassword(passwordPayload: ViewDeletePasswordDto): Promise<any>{
    return this._passwordRepository.delete(passwordPayload.id);
  }

  /**
   * View password
   *
   * @param passwordPayload {ViewDeletePasswordDto}
   *
   * @return {Promise<UserPasswordEntity>}
   *
  */
  public viewPassword(passwordPayload: ViewDeletePasswordDto): Promise<UserPasswordEntity>{
    return this._passwordRepository.findOne(passwordPayload.id);
  }

  /**
   * Save public key
   *
   * @param payload
   *
   * @return {Promise}
  */
  public savePublicKey(payload: any){
    return new Promise<any>((resolve, reject) => {
      const {id} = payload;
      this._userRepository.update(id, {public_key: payload.publicKey})
        .then(res => resolve(res))
        .catch(e => reject(e))
    })
  }
}
