import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,

} from "@nestjs/websockets";
import { Logger, UsePipes } from "@nestjs/common";
import {Server, Socket} from 'socket.io';
import { UserService } from "./user.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { LoginUserDto } from "./dto/login-user.dto";
import { CreatePasswordDto } from "./dto/create-password.dto";
import { EditPasswordDto } from "./dto/edit-password.dto";
import { ViewDeletePasswordDto } from "./dto/view-delete-password.dto";
import { GatewayPayloadTransformPipe } from "./pipes/gateway-payload-transform.pipe";
import { CryptHandlerService } from "./crypt-handler.service";

@WebSocketGateway()
export class UserGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect{

  /** Logger instance */
  private _logger = new Logger('UserGatewayLogger');


  /**
   * Create a new instance from UserGateway
   *
   * @constructor
   *
   * @param _userService {UserService}
   * @param _cryptHandlerService {CryptHandlerService}
   */
  constructor(
    private readonly _userService: UserService,
    private readonly _cryptHandlerService: CryptHandlerService
  ) {

  }

  /**
   * Handle Initialization of websocket server
   *
   * @override
   *
   * @param server {Server}
   *
   * @return {void}
   */
  afterInit(server: Server): void {
    this._logger.log('WS Server Initialized');
  }

  /**
   * Handle client connection to websocket server
   *
   * @override
   *
   * @param client {Socket}
   * @param args {[]}
   *
   * @return {void}
   *
   */
  handleConnection(client: Socket, ...args: any[]): void {
    // Send public key to user.
    client.emit('getCR', {cr: this._cryptHandlerService.certificateAuthority})
    // Send keys to client
    this._cryptHandlerService.generateClientKeyPair()
      .then(keyPair => {
          client.emit('getClientKeyPair', keyPair)
      })
    this._logger.log(`Client ${client.id} is connected`);
  }

  /**
   * Handle client disconnect form websocket server.
   *
   * @override
   *
   * @param client {Socket}
   *
   * @return {void}
   *
   */
  handleDisconnect(client: Socket): void {
    this._logger.log(`Client ${client.id} is disconnected`);
  }

  /**
   * Save a new user to database
   *
   * @param client {Socket}
   * @param payload {CreateUserDto}
   *
   * @return {Promise<void>}
   *
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('createNewUser')
  async handleCreateNewUserMessage(client: Socket, payload: CreateUserDto): Promise<void> {
    // Create a new user by passed payload.
     this._userService.createUser(payload)
      .then(user => client.emit('userCreated', this._cryptHandlerService.sendResponse('Create new user success', user, true)))
      .catch(e => client.emit('userCreated', this._cryptHandlerService.sendErrorResponse(e.message)));
  }

  /**
   * Login old user
   *
   * @param client {Socket}
   * @param payload {LoginUserDto}
   *
   * @return {Promise<void>}
   *
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('loginOldUser')
  async loginOldUser(client: Socket, payload: LoginUserDto): Promise<void>{
    this._logger.log(payload.name);
    // Login user.
    this._userService.loginOldUser(payload)
      .then(res => {
          if (res){
            client.emit('loginResponse', this._cryptHandlerService.sendResponse('Login success', res, true));
          }else {
            client.emit('loginResponse', this._cryptHandlerService.sendErrorResponse('Credentials not valid!!'));
          }
      }).catch(e => client.emit('loginResponse', this._cryptHandlerService.sendErrorResponse(e.message)))
  }

  /**
   * Create a new password for user.
   *
   * @param client {Socket}
   * @param payload {CreatePasswordDto}
   *
   * @return {Promise<void>}
   *
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('createNewPassword')
  async createNewPassword(client: Socket, payload: CreatePasswordDto): Promise<void>{
    this._userService.createNewPassword(payload)
      .then(res => client.emit('createNewPasswordResponse', this._cryptHandlerService.sendResponse('create new password success', res, true)))
      .catch(e => client.emit('createNewPasswordResponse', this._cryptHandlerService.sendErrorResponse(e.message)));
  }

  /**
   * Update a password.
   *
   * @param client {Socket}
   * @param payload {EditPasswordDto}
   *
   * @return {Promise<void>}
   *
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('updatePassword')
  async updatePassword(client: Socket, payload: EditPasswordDto): Promise<void>{
    this._userService.editPassword(payload)
      .then(res => client.emit('updatePasswordResponse', this._cryptHandlerService.sendResponse('Update password success', res, true)))
      .catch(e => client.emit('updatePasswordResponse', this._cryptHandlerService.sendErrorResponse(e.message)));
  }

  /**
   * Delete password
   *
   * @param client {Socket}
   * @param payload {ViewDeletePasswordDto}
   *
   * @return {Promise<void>}
   *
  */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('deletePassword')
  async deletePassword(client: Socket, payload: ViewDeletePasswordDto): Promise<void>{
    this._userService.deletePassword(payload)
      .then(res => client.emit('deletePasswordResponse',  this._cryptHandlerService.sendResponse('Delete password success', res, true)))
      .catch(e => client.emit('deletePasswordResponse', this._cryptHandlerService.sendErrorResponse(e.message)));
  }

  /**
   * View password
   *
   * @param client {Socket}
   * @param payload {ViewDeletePasswordDto}
   *
   * @return {Promise<void>}
   *
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('viewPassword')
  async viewPassword(client: Socket, payload: ViewDeletePasswordDto): Promise<void>{
    this._userService.viewPassword(payload)
      .then(res => client.emit('viewPasswordResponse', this._cryptHandlerService.sendResponse('View Password success', res, true)))
      .catch(e => client.emit('viewPasswordResponse', this._cryptHandlerService.sendErrorResponse(e.message)));
  }

  /**
   * Save client public key.
   *
   * @param {Socket} client
   * @param payload
   *
   * @return {Promise<void>}
   */
  @UsePipes(GatewayPayloadTransformPipe)
  @SubscribeMessage('savePublicKey')
  async savePublicKey(client: Socket, payload){
  }
}

