import { Injectable, HttpService  } from "@nestjs/common";
import { ResponseShape } from "./dto/ResponseShape";
const { generateKeyPair } = require('crypto');
import KeyPairConfig from '../config/key-pair.config'

@Injectable()
export class CryptHandlerService {

  /** Server public key  */
  private _publicKey: string

  /** Server private key */
  private _privateKey: string

  /** Certificate authority */
  private _certificateAuthority: string

  /**
   * Create a new instance from CryptHandlerService
   *
   * @constructor
   *
  */
  constructor(
    private http: HttpService
  ) {
    // Generate key pair
    this.generateKeyPair()
  }

  /**
   * Get public key
   *
   * @return {string}
  */
  get publicKey(): string{
    return this._publicKey
  }

  /**
   * Get certificate authority
   *
   * @return {string}
  */
  get certificateAuthority(): string{
    return this._certificateAuthority;
  }

  /**
   * Generate key pair
   *
   * @return {void}
   *
  */
  generateKeyPair(): void{
    generateKeyPair(
      KeyPairConfig.algorithm,
      KeyPairConfig.options,
      (err, puk, prk) => {
        if (err) {
          console.log(err)
        }else {
          this._privateKey = prk.toString('hex')
          this._publicKey = puk.toString('hex')
          this.sendCSR()
        }
      }
    )
  }

  /**
   * Generate client key pair
   *
   *
   * @return Promise<{publicKey: string, privateKey: string}>
   */
  generateClientKeyPair(): Promise<{publicKey: string, privateKey: string}>{
    return new Promise((resolve, reject) => {
      generateKeyPair(
        KeyPairConfig.algorithm,
        KeyPairConfig.options,
        (err, puk, prk) => {
          if (err) {
            reject(err)
          }else {
            resolve({publicKey: puk, privateKey: prk})
          }
        }
      )
    })
  }

  /**
   * Send CSR
   *
   * @return {void}
   **/
  sendCSR(): void{
    this.http.post('http://localhost:8000/api/generate_cr', {
        public_key: this._publicKey,
        identifier: 'localhost:3000'
    }).subscribe(
      next => {
        this._certificateAuthority = next.data.certificate
      }, err => {
        console.log(err)
      }
    )
  }

  /**
   *
   * Verify cr
   *
   * @param {string} identifier
   *
   * @return {Promise<boolean>}
   */
  verifyCR(identifier: string):Promise<boolean>{
    return new Promise<boolean>((resolve, reject) => {
      this.http.post('http://localhost:8000/api/verify_cr', {
        certificate: this._certificateAuthority,
        identifier
      }).subscribe(
        next => {
          resolve(next.data.state)
        }, error => {
          reject(error)
        }
      )
    })
  }


  /**
   * Send response
   *
   * @param message {string}
   * @param data
   * @param state {boolean}
   *
   * @return {ResponseShape}
   *
   */
  sendResponse(message: string, data: any, state: boolean): ResponseShape{
    // Return results.
    return {results: ""};
  }

  /**
   * Send error response
   *
   * @param message {string}
   *
   * @return {ResponseShape}
   *
   */
  sendErrorResponse(message: string): ResponseShape{
    return this.sendResponse(message, [], false);
  }
}
