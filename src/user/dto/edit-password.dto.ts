import { CreatePasswordDto } from "./create-password.dto";

export class EditPasswordDto extends CreatePasswordDto{
  id: number;
}
