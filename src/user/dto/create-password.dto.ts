export class CreatePasswordDto{
  user_id: number;
  title: string;
  username: string;
  password: string;
  description: string;
}
