const handleResponse = (message, data, state) => {
  return {message, data, state}
}

const errorHandler = (message) => {
  return handleResponse(message, [], false);
}

export {
  handleResponse,
  errorHandler
}
